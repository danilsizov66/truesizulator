package com.example.sizovde_3_1p11_calculator;

import android.widget.Button;

import org.mariuszgromada.math.mxparser.Expression;

import java.util.ArrayList;

public class Logic {
    String mainStr;
    double Result;
    Expression e;

    void Result()
    {
        e.setExpressionString(mainStr);
        Result = e.calculate();
        mainStr = Double.toString(Result);
    }
    //очистка
    public void Clear()
    {
        mainStr = "";
    }
    public Logic()
    {
        mainStr = "";
        e = new Expression();
    }
}
