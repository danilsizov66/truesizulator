package com.example.sizovde_3_1p11_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button Plus, Minus, Bisect, Divide, Result, Clear;
    Button Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine;
    TextView MainField;
    Logic SomeLogic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainField = findViewById(R.id.textField2);
        SomeLogic = new Logic();
        MainField.setText("0");
        assignID(Zero, R.id.b_zero);
        assignID(One,R.id.b_1);
        assignID(Two,R.id.b_2);
        assignID(Three,R.id.b_3);
        assignID(Four,R.id.b_4);
        assignID(Five,R.id.b_5);
        assignID(Six,R.id.b_6);
        assignID(Seven,R.id.b_7);
        assignID(Eight,R.id.b_8);
        assignID(Nine,R.id.b_9);
        assignID(Clear,R.id.b_clear);
        assignID(Plus,R.id.b_plus);
        assignID(Minus,R.id.b_minus);
        assignID(Divide,R.id.b_multiple);
        assignID(Bisect,R.id.b_divide);
        assignID(Result,R.id.b_result);
        assignID(Clear,R.id.b_clear);
    }

    void assignID(Button b, int button_id) {

        b = findViewById(button_id);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Button button = (Button) view;
        switch (button.getId())
        {
            case R.id.b_clear:
                SomeLogic.Clear();
                MainField.setText("0");
                break;
            case R.id.b_result:
                SomeLogic.Result();
                MainField.setText(SomeLogic.mainStr);
                break;
            default:
                SomeLogic.mainStr += button.getText();
                MainField.setText(SomeLogic.mainStr);
                break;
        }
    }
}
